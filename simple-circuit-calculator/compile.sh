#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
BUILD_DIRECTORY=build
PACKAGE=SimpleCircuitCalculator
FILENAME_ARGUMENT=$1
FILENAME=${FILENAME_ARGUMENT:='simple-circuit-calculator.jar'}

cd $SCRIPT_DIR
mkdir -p $BUILD_DIRECTORY
cp manifest.mf $BUILD_DIRECTORY/manifest.mf
javac -d ./$BUILD_DIRECTORY src/$PACKAGE/*.java
cd $BUILD_DIRECTORY
jar -cvfm ../$FILENAME manifest.mf $PACKAGE
cd ..
chmod +x $FILENAME
rm -rf $BUILD_DIRECTORY
