# Simple Circuit Calculator

Application to compute for the results of any variable in a simple circuit based
on the **Ohm’s Law** Physics equation

Includes auto-fill functionality

![Simple Circuit Calculator](documentation/simple-circuit-calculator.gif)

# Try it out!

## Run pre-compiled executable from the command line

```sh
java -jar simple-circuit-calculator.jar
```

> You may also navigate to the application directory through the file explorer
> to open the executable file

## Compile from source

```sh
# Compile to default filename (simple-circuit-calculator.jar)
./compile.sh

# Compile to custom filename
./compile.sh filename.jar
```
