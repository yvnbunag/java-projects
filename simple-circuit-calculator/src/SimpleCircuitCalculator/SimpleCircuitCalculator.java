package SimpleCircuitCalculator;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

class mainframe extends JFrame implements ActionListener,FocusListener{
    JFrame project = new JFrame("Simple Circuit Calculator");
    JRadioButton series = new JRadioButton("Series Connection");
    JRadioButton parallel = new JRadioButton("Parallel Connection");

    JButton solve = new JButton("Solve for Total");
    JButton clear = new JButton("Clear");
    JButton clear1 = new JButton("Clear All");
    JButton ivr1 = new JButton("I1 = V1/R1");
    JButton ivr2 = new JButton("I2 = V2/R2");
    JButton vir1 = new JButton("V1 = I1*R1");
    JButton vir2 = new JButton("V2 = I2*R2");
    JButton rvi1 = new JButton("R1 = V1/I1");
    JButton rvi2 = new JButton("R2 = V2/I2");
    JLabel current =  new JLabel("Current1 (I)");
    JLabel voltage =  new JLabel("Voltage1 (V)");
    JLabel resistor = new JLabel("Resistor1 (Ω)");
    JLabel system = new JLabel("System Message");

    JTextField cur1 = new JTextField(6);
    JTextField cur2 = new JTextField(6);
    JTextField vol1 = new JTextField(6);
    JTextField vol2 = new JTextField(6);
    JTextField res1 = new JTextField(6);
    JTextField res2 = new JTextField(6);
    JTextField voltot = new JTextField(10);
    JTextField curtot = new JTextField(10);
    JTextField restot = new JTextField(10);
    JTextField message = new JTextField(18);

    int ctr1=0,ctr2=0,width=350;
    String corba = "";

    boolean serSelect = true,parSelect = false;


    mainframe(){
        project.setBounds(530,30,width,680);
        project.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        project.setResizable(false);
        project.setLayout(new FlowLayout());
        project.add(series);
        project.add(parallel);

        series.setSelected(true);


        JLabel space = new JLabel("----------------------------------------------------------------------------");
        project.add(space);
        project.add(current);
        project.add(cur1);
        current = new JLabel("Current2 (I)");
        project.add(current);
        project.add(cur2);
        project.add(voltage);
        project.add(vol1);
        voltage = new JLabel("Voltage2 (V)");
        project.add(voltage);
        project.add(vol2);
        project.add(resistor);
        project.add(res1);
        resistor = new JLabel("Resistor2 (Ω)");
        project.add(resistor);

        project.add(res2);
        project.add(ivr1);
        space = new JLabel("        ");
        project.add(space);
        project.add(ivr2);
        project.add(vir1);
        space = new JLabel("        ");
        project.add(space);
        project.add(vir2);
        project.add(rvi1);
        space = new JLabel("        ");
        project.add(space);
        project.add(rvi2);
        space = new JLabel("----------------------------------------------------------------------------");
        project.add(space);
        project.add(solve);
        project.add(clear);
        project.add(clear1);
        space = new JLabel("----------------------------------------------------------------------------");
        project.add(space);
        current = new JLabel("        Total Current");
        project.add(current);
        project.add(curtot);
        voltage = new JLabel("        Total Voltage");
        project.add(voltage);
        project.add(voltot);
        resistor = new JLabel("             Total Resitance");
        project.add(resistor);
        project.add(restot);
        space = new JLabel("----------------------------------------------------------------------------");
        project.add(space);
        project.add(system);
        project.add(message);
        space = new JLabel("----------------------------------------------------------------------------");
        project.add(space);

        int noteWidth = width - 20;
        String noteMessage =
            "<html><body style='text-align:center;width: " + noteWidth + ";'><p>"
            + "*Circuit values not matching the answer based on Ohm's Law "
            + "Formula(I = V/R) will not be accepted. Text Field selected "
            + "during this error will automatically adjust to the other two "
            + "given values."
            + "<br/>"
            + "**When circuit type is series, Current values will "
            + "automatically adjust based on the given value but will not "
            + "adjust based on Ohm's Law Formula (I=V/R)."
            + "<br/>"
            + "***When circuit type is parallel, Voltage values will "
            + "automatically adjust based on the given value but will not "
            + "adjust based on Ohm's Law Formula (V=IR)."
            + "</p><body></html>";
        JLabel note = new JLabel(noteMessage);
        project.add(note);

        series.addActionListener(this);
        parallel.addActionListener(this);
        solve.addActionListener(this);
        clear.addActionListener(this);
        ivr1.addActionListener(this);
        ivr2.addActionListener(this);
        vir1.addActionListener(this);
        vir2.addActionListener(this);
        clear1.addActionListener(this);
        rvi1.addActionListener(this);
        rvi2.addActionListener(this);

        cur1.addFocusListener(this);
        vol1.addFocusListener(this);
        res1.addFocusListener(this);
        cur2.addFocusListener(this);
        vol2.addFocusListener(this);
        res2.addFocusListener(this);
        project.setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        try{
        Object source = e.getSource();
        if(source==ivr1){
            if(res1.getText().length()!=0 && vol1.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol1.getText())/Double.parseDouble(res1.getText()));
               cur1.setText(mis);
               error("I1 set manually thru V1/R1");
               if(serSelect==true){
                   cur2.setText(mis);
               }
           }
            else{
                error("manual set failed, error in  values");
            }

           }
        if(source==ivr2){
            if(res2.getText().length()!=0 && vol2.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol2.getText())/Double.parseDouble(res2.getText()));
               cur2.setText(mis);
               error("I2 set manually thru V2/R2");
               if(serSelect==true){

                   cur1.setText(mis);
               }
           }
            else{
                error("manual set failed, error in  values");
            }
           }

        if(source==vir1){
             if(cur1.getText().length()!=0 && res1.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(cur1.getText())*Double.parseDouble(res1.getText()));
               vol1.setText(mis);
               error("V1 set manually thru I1*R1");
               if(parSelect==true){
                   vol2.setText(mis);
               }
           }
             else{
                error("manual set failed, error in  values");
            }
        }

        if(source==vir2){
             if(cur2.getText().length()!=0 && res2.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(cur2.getText())*Double.parseDouble(res2.getText()));
               vol2.setText(mis);
               error("V2 set manually thru I2*R2");
               if(parSelect==true){
                   vol1.setText(mis);
               }
           }
             else{
                error("manual set failed, error in  values");
            }
        }

        if(source==rvi1){
            if(cur1.getText().length()!=0 && vol1.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol1.getText())/Double.parseDouble(cur1.getText()));
               res1.setText(mis);
               error("R1 set manually thru V1/I1");
           }
            else{
                error("manual set failed, error in  values");
            }
        }

        if(source==rvi2){
            if(cur2.getText().length()!=0 && vol2.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol2.getText())/Double.parseDouble(cur2.getText()));
               res2.setText(mis);
               error("R2 set manually thru V2/I2");
           }
             else{
                error("manual set failed, error in  values");
            }
        }

        if(source == series){
            if(serSelect==true){
                serSelect = false;
                error("Series connection type unselected");
            }
            else{
                serSelect = true;
                parallel.setSelected(false);
                parSelect = false;
                error("Series connection type selected");
            }
        }

        if(source == parallel){
            if(parSelect==true){
                parSelect = false;
                error("Parallel connection type unselected");
            }
            else{
                parSelect = true;
                series.setSelected(false);
                serSelect = false;
                error("Parallel connection type selected");
            }
        }
        if(source == solve){
        int ctrup1 = 0, ctrup2 = 0;
        if(cur1.getText().length()!= 0){
            ctrup1++;
        }
        if(vol1.getText().length()!= 0){
            ctrup1++;
        }
        if(res1.getText().length()!= 0){
            ctrup1++;
        }
        if(cur2.getText().length()!= 0){
            ctrup2++;
        }
        if(vol2.getText().length()!= 0){
            ctrup2++;
        }
        if(res2.getText().length()!= 0){
            ctrup2++;
        }

        ctr1 = ctrup1;
        ctr2 = ctrup2;


        try{
        if(ctr1<3 || ctr2<3){
            throw new RuntimeException("");
        }
        }
        catch(RuntimeException a){
               String ctr1mes="";
               String ctr2mes="";

               if(2-ctr1!=-1){
                   ctr1mes = String.valueOf(3-ctr1);
               }
               else{
                   ctr1mes = "ok";
               }

               if(2-ctr2!=-1){
                   ctr2mes = String.valueOf(3-ctr2);
               }
               else{
                   ctr2mes = "ok";
               }

               error("Missing Fields: Cir1: "+ctr1mes+" Cir2: "+ctr2mes);
                }
        if(ctr1>0 && ctr2 >0){

            }
            //solve

            if(parSelect==false&&serSelect==false){
                error("Mark one Circuit type(S/P)");
            }
            else{
               solver();
               isCorrect(corba);
            }

        }
        if(source==clear){
            message.setText("");
            cur1.setText("");
            vol1.setText("");
            res1.setText("");
            cur2.setText("");
            vol2.setText("");
            res2.setText("");
        }
        if(source==clear1){
            message.setText("");
            cur1.setText("");
            vol1.setText("");
            res1.setText("");
            cur2.setText("");
            vol2.setText("");
            res2.setText("");
            voltot.setText("");
            curtot.setText("");
            restot.setText("");
            message.setText("");
        }
        }
        catch(Exception a){
            error("Field value error found");
        }

        }




    @Override
    public void focusGained(FocusEvent e) {
       try{
       Object source = e.getSource();
       if(source==res1){
           if(cur1.getText().length()!=0 && vol1.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol1.getText())/Double.parseDouble(cur1.getText()));
               res1.setText(mis);
               error("R1 auto-set thru V1/I1");

           }
       }


       if(serSelect==false){
       if(source==cur1){
           if(res1.getText().length()!=0 && vol1.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol1.getText())/Double.parseDouble(res1.getText()));
               cur1.setText(mis);
               error("I1 auto-set thru V1/R1");
           }
           }

           if(source==cur2){
           if(res2.getText().length()!=0 && vol2.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol2.getText())/Double.parseDouble(res2.getText()));
               cur2.setText(mis);
               error("I2 auto-set thru V2/R2");
           }

           }
       }

         if(parSelect==false) {
        if(source==vol1){
           if(cur1.getText().length()!=0 && res1.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(cur1.getText())*Double.parseDouble(res1.getText()));
               vol1.setText(mis);
               error("V1 auto-set thru I1*R1");
           }
       }
         if(source==vol2){
           if(cur2.getText().length()!=0 && res2.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(cur2.getText())*Double.parseDouble(res2.getText()));
               vol2.setText(mis);
               error("V2 auto-set thru I2*R2");
           }
       }
         }

       if(source==res2){
           if(cur2.getText().length()!=0 && vol2.getText().length()!=0 ){
               String mis = String.valueOf(Double.parseDouble(vol2.getText())/Double.parseDouble(cur2.getText()));
               res2.setText(mis);
              error("R2 auto-set thru V2/I2");
           }
       }


       }

       catch(Exception a){
           error("Field value error found");
       }

    }


    public void focusLost(FocusEvent e) {
        try{
        Object source = e.getSource();
        if(source==cur1){
            if(serSelect==true){
                cur2.setText(cur1.getText());
                error("I2 auto-set thru I1=I2(series)");
            }
        }
        if(source==cur2){
            if(serSelect==true){
                cur1.setText(cur2.getText());
                error("I1 auto-set thru I1=I2(series)");
            }
        }
        if(source==vol1){
            if(parSelect==true){
                vol2.setText(vol1.getText());
                error("V2 auto-set thru V1=V2(parallel)");
            }
        }
        if(source==vol2){
            if(parSelect==true){
                vol1.setText(vol2.getText());
                error("V1 auto-set thru V1=V2(parallel)");
            }
        }
        }catch(Exception a){
             error("Field value error found");
        }
    }

    private void error(String mes) {
       message.setText(mes);
    }

    private void isCorrect(String meserr){

        int errtype=0;
        if(Double.parseDouble(cur1.getText())!=Double.parseDouble(vol1.getText())/Double.parseDouble(res1.getText())){
             meserr="Invalid Data in Cir 1(I=V/R:false)";
             errtype++;
        }
        else if(Double.parseDouble(cur2.getText())!=Double.parseDouble(vol2.getText())/Double.parseDouble(res2.getText())){
            if(errtype==0){
             meserr="Invalid Data in Cir 2(I=V/R:false)";
            }
            if(errtype==1){
              meserr="Invalid Data in Cir 1 & 2";
            }

        }
        error(meserr);
        }

    private void solver(){
        String voltagetotal="";
        String currenttotal="";
        String resistortotal="";
        if(parSelect==true){
        if(parSelect==true && (Double.parseDouble(vol1.getText()))==Double.parseDouble(vol2.getText())){
            double v,c,r;
            v =  Double.parseDouble(vol1.getText());
            voltagetotal = String.valueOf(String.format("%.2f",v));
            voltot.setText(voltagetotal);
            c =  Double.parseDouble(cur1.getText())+Double.parseDouble(cur2.getText());
            currenttotal = String.valueOf(String.format("%.2f",c));
            r =  v/c;
            resistortotal = String.valueOf(String.format("%.2f",r));

            voltot.setText(voltagetotal+" V (Constant)");
            curtot.setText(currenttotal+" I");
            restot.setText(resistortotal+" ‎Ω");

            corba = "Successfully solved for values";
        }
        else{
            corba = "Voltages must be constant";
        }
        }
        else{
        if(serSelect==true&& (Double.parseDouble(cur1.getText()))==Double.parseDouble(cur2.getText())){
            double v,c,r;
            c =  Double.parseDouble(cur1.getText());
            currenttotal = String.valueOf(String.format("%.2f",c));
            voltot.setText(voltagetotal);
            v =  Double.parseDouble(vol1.getText())+Double.parseDouble(vol2.getText());
            voltagetotal = String.valueOf(String.format("%.2f",v));
            r =  v/c;
            resistortotal = String.valueOf(String.format("%.2f",r));


            curtot.setText(currenttotal+" I (Constant)");
            voltot.setText(voltagetotal+" V");
            restot.setText(resistortotal+" ‎Ω");

            corba = "Successfully solved for values";
        }
        else{
            corba = "Currents must be constant";
        }
        }
        }
    }

public class SimpleCircuitCalculator {
    public static void main(String[] args) {
        mainframe calc = new mainframe();
    }
}
