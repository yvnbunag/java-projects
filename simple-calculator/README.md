# Simple Calculator

Application to compute basic arithmetic operations

Includes type/click input, clear, input negation and system message
functionalities

![Simple Calculator](documentation/simple-calculator.gif)

# Try it out!

## Run pre-compiled executable from the command line

```sh
java -jar simple-calculator.jar
```

> You may also navigate to the application directory through the file explorer
> to open the executable file

## Compile from source

```sh
# Compile to default filename (simple-calculator.jar)
./compile.sh

# Compile to custom filename
./compile.sh filename.jar
```
