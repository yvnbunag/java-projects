
package SimpleCalculator;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class calc extends JFrame implements ActionListener{
    JFrame box = new JFrame("Calculator");
    JTextField out = new JTextField(18);
    JButton c1 = new JButton(" 1 ");
    JButton c2 = new JButton(" 2 ");
    JButton c3 = new JButton(" 3 ");
    JButton c4 = new JButton(" 4 ");
    JButton c5 = new JButton(" 5 ");
    JButton c6 = new JButton(" 6 ");
    JButton c7 = new JButton(" 7 ");
    JButton c8 = new JButton(" 8 ");
    JButton c9 = new JButton(" 9 ");
    JButton c0 = new JButton(" 0 ");
    JButton nega = new JButton(" (-) ");
    JButton cdot = new JButton(" . ");
    JButton clear = new JButton(" CLEAR ");
    JButton erase = new JButton("  DEL  ");
    JButton add = new JButton (" + ");
    JButton sub = new JButton (" - ");
    JButton mul = new JButton(" * ");
    JButton div = new JButton(" / ");
    JButton equal = new JButton(" = ");
    JLabel message = new JLabel("System Message");
    JTextField sys = new JTextField(9);

    boolean therenega = false;
    boolean isnega = false;
    boolean opeyes = false;
    boolean thereminus = false;
    boolean dotyes = false;
    boolean nosecond = false;
    int opectr=0;
    calc(){
        box.setResizable(false);
        box.setBounds(530,200,250,245);
        box.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        box.setLayout(new FlowLayout());
        box.add(out);
        box.add(clear);
        box.add(erase);
        box.add(add);
        box.add(c1);
        box.add(c2);
        box.add(c3);
        box.add(sub);
        box.add(c4);
        box.add(c5);
        box.add(c6);
        box.add(mul);
        box.add(c7);
        box.add(c8);
        box.add(c9);
        box.add(div);
        box.add(nega);
        box.add(c0);
        box.add(cdot);
        box.add(equal);
        box.add(message);
        box.add(sys);

        c1.addActionListener(this);
        c2.addActionListener(this);
        c3.addActionListener(this);
        c4.addActionListener(this);
        c5.addActionListener(this);
        c6.addActionListener(this);
        c7.addActionListener(this);
        c8.addActionListener(this);
        c9.addActionListener(this);
        c0.addActionListener(this);
        clear.addActionListener(this);
        erase.addActionListener(this);
        sub.addActionListener(this);
        add.addActionListener(this);
        mul.addActionListener(this);
        div.addActionListener(this);
        equal.addActionListener(this);
        cdot.addActionListener(this);
        nega.addActionListener(this);

        box.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source==c1){
            out.setText(out.getText()+"1");
        }
        if(source==c2){
            out.setText(out.getText()+"2");
        }
        if(source==c3){
            out.setText(out.getText()+"3");
        }
        if(source==c4){
            out.setText(out.getText()+"4");
        }
        if(source==c5){
            out.setText(out.getText()+"5");
        }
        if(source==c6){
            out.setText(out.getText()+"6");
        }
        if(source==c7){
            out.setText(out.getText()+"7");
        }
        if(source==c8){
            out.setText(out.getText()+"8");
        }
        if(source==c9){
            out.setText(out.getText()+"9");
        }

        if(source==c0){
            out.setText(out.getText()+"0");
        }
        if(source==nega){
            if(isnega==false){
            out.setText("-"+out.getText());
            isnega=true;
            }

            else{
            int c=1;
            char a = 'a';
            String delete1 ="";
                try{
                    while(true){
                    a = out.getText().charAt(c);
                    delete1+=a;
                    c++;
                    }
                    }
             catch(StringIndexOutOfBoundsException ae){

            }
            isnega=false;
            out.setText(delete1);
             }


            }


        if(source==add||source==sub||source==mul||source==div){
            boolean inyes = false;
            int ctr=0;
            String what="a";
            String compareadd = "+";
            String comparesub = "-";
            String comparemul = "*";
            String comparediv = "/";
            try{
            while(true){
                what = String.valueOf(out.getText().charAt(ctr));
                if(what.charAt(0)==compareadd.charAt(0)){
                    inyes=true;
                    sys.setText("1 operator only");
                }
                if(what.charAt(0)==comparesub.charAt(0)){
                    therenega=true;
                    if(therenega==true&& ctr==0){
                        therenega = false;
                    }
                    if(therenega==true){
                       inyes=true;
                       sys.setText("1 operator only");
                    }
                }
                if(what.charAt(0)==comparemul.charAt(0)){
                    inyes=true;
                    sys.setText("1 operator only");
                }
                if(what.charAt(0)==comparediv.charAt(0)){
                    inyes=true;
                    sys.setText("1 operator only");
                }
                ctr++;

            }
            }

            catch(StringIndexOutOfBoundsException a){
            }
            if(inyes==true){
                opeyes = true;
            }
            else{
                opeyes = false;
            }
            if(opeyes == false){
                if(source==add){
            out.setText(out.getText()+"+");
                sys.setText("Opr: Addition");
                }
                if(source==sub){
            out.setText(out.getText()+"-");
                sys.setText("Opr: Subtraction");
                }
                if(source==mul){
            out.setText(out.getText()+"*");
                sys.setText("Opr: Multiplication");
                }
                if(source==div){
            out.setText(out.getText()+"/");
                sys.setText("Opr: Division");
                }
            }
        }


        if(source==cdot){
        dotyes=false;
            int ctr=0;
            String what="a";
            String compare = ".";

            try{
            while(true){
                what = String.valueOf(out.getText().charAt(ctr));
                if(what.charAt(0)==compare.charAt(0)){
                    dotyes=true;
                    sys.setText("invalid repetition");

                    }
                if(String.valueOf(what).equals("+")==true||String.valueOf(what).equals("-")==true
                        ||String.valueOf(what).equals("/")==true||String.valueOf(what).equals("*")==true){
                        dotyes=false;
                }

                ctr++;

            }
            }

            catch(StringIndexOutOfBoundsException a){

            }
            if(dotyes == false){
            out.setText(out.getText()+".");
            }

        }
        if(source==erase){
            int c=0;
            int adder = 0;
            char a = 'a';
            String delete1 ="";
            try{

            while(true){
                a = out.getText().charAt(c);
                c++;
            }
            }
            catch(StringIndexOutOfBoundsException ae){
                 if(out.getText().length()==0){
                 sys.setText("Nothing to delete");
                 }
                    sys.setText(out.getText().charAt(c-1)+" removed");
                    while(adder<(c-1)){
                    a = out.getText().charAt(adder);
                    delete1+=a;
                    adder++;
            }

            out.setText(delete1);
                    }
        }
        if(source==clear){
            out.setText("");
            sys.setText("Cleared");
        }

        if(source==equal){
            opectr=0;
            String numA ="";
            String numB="";
            String numC="";
            int ctr = 0;
            int operator=0;
            String what="a";
            try{
            try{
            while(true){

                 what = String.valueOf(out.getText().charAt(ctr));
                 if(String.valueOf(what).equals("+")==true){
                     operator=1;
                     opectr++;
                 }
                 if(String.valueOf(what).equals("-")==true){
                     if(ctr==0){
                         therenega=false;
                     }
                     else{
                     opectr++;
                     operator=2;
                     }
                 }
                 if(String.valueOf(what).equals("*")==true){
                     opectr++;
                     operator=3;
                 }
                 if(String.valueOf(what).equals("/")==true){
                     opectr++;
                     operator=4;
                 }
                 if(String.valueOf(what).equals("+")==false||String.valueOf(what).equals("-")==false
                        ||String.valueOf(what).equals("/")==false||String.valueOf(what).equals("*")==false){
                          if(operator==0){
                              numA+=String.valueOf(what);
                          }
                          if(operator>0){
                              numC+=String.valueOf(what);
                          }
                }

                ctr++;
            }
            }
            catch(StringIndexOutOfBoundsException qwe){
                try{
                    ctr=1;
                while(true){
                    numB+=numC.charAt(ctr);
                    ctr++;
                }
                }
                catch(StringIndexOutOfBoundsException asd){

                }

            }
            nosecond=false;
            String a = out.getText();
            if(numB.length()==0){
                nosecond=true;

            }
            double num1 = Double.parseDouble(numA);

            double num2 = Double.parseDouble(numB);
            String john = "-";
            String ian = String.valueOf(solve(num1,num2,operator));
            out.setText(ian);
           if(String.valueOf(ian.charAt(0)).equals("-")==true){

                isnega=true;

            }
            sys.setText("Success");
            }
            catch(NumberFormatException zxc){

                sys.setText("not an equation");
                if(nosecond!=true){
                sys.setText("Invalid Equation");
                }
                if(opectr>1){
                sys.setText("1 operator only");
                }

        }
    }
    }

    private double solve(double num1, double num2, int operator) {
        double ret=0;
      switch(operator){
          case 1: ret = num1+num2;
              break;
          case 2: ret = num1-num2;
              break;
          case 3: ret = num1*num2;
              break;
          case 4: ret = num1/num2;
              break;
      }
      if(ret<0){
          isnega=true;
              }
      return ret;
    }


}


public class SimpleCalculator {

    public static void main(String[] args) {
        calc ian = new calc();
    }

}
