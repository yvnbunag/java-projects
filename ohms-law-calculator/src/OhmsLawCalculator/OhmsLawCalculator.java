package OhmsLawCalculator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class calcu extends JFrame implements ActionListener{

    JFrame OFrame = new JFrame("Ohm's Law Calculator");
    JLabel Front = new JLabel ("Enter any of the two, to find the other one which is missing.",SwingConstants.CENTER);
    JLabel vol = new JLabel("Enter Voltage(V): ",SwingConstants.CENTER);
    JTextField volans = new JTextField(20);
    JLabel amp = new JLabel ("Enter Ampere(I):  ",SwingConstants.CENTER);
    JTextField ampans = new JTextField(20);
    JLabel res= new JLabel("Enter Resistance(Ω): ",SwingConstants.CENTER);
    JTextField resans = new JTextField(20);
    JLabel answerto = new JLabel();
    JLabel space = new JLabel("ANSWER: ");
    final int x = 190;
    final int y= 420;

    JButton Calc = new JButton ("CALCULATE");

    calcu(){
        OFrame.setSize(y,x);
        OFrame.setResizable(false);
        OFrame.setVisible(true);
        OFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        OFrame.setLayout(new FlowLayout());
        OFrame.add(Front);
        OFrame.add(vol);
        OFrame.add(volans);
        OFrame.add(amp);
        OFrame.add(ampans);
        OFrame.add(res);
        OFrame.add(resans);

        OFrame.add(Calc);

        Calc.addActionListener(this);
        Calc.setToolTipText("CLICK THIS TO SEE THE RESULT");
        volans.setToolTipText("Enter the value of Voltage");
        ampans.setToolTipText("Enter the value of Ampere");
        resans.setToolTipText("Enter the value of Resistance");

    }


    public void actionPerformed(ActionEvent e) {
        String volt = volans.getText();
        String ampe = ampans.getText();
        String resi = resans.getText();



        Double answer;
        if (volt.equals("")){
            Double amper = Double.parseDouble(ampe);
            Double resis = Double.parseDouble(resi);
            volt="";
            answer=amper*resis;
            OFrame.remove(answerto);
            OFrame.add(space);
            answerto = new JLabel("The Voltage is "+answer);
            OFrame.add(answerto);

            OFrame.setVisible(false);
            OFrame.setVisible(true);
        }
        if (ampe.equals("")){
            ampe="";
            Double volta = Double.parseDouble(volt);
            Double resis = Double.parseDouble(resi);
            answer=volta/resis;
            OFrame.remove(answerto);
            OFrame.add(space);
            answerto = new JLabel("The Ampere is "+answer);
            OFrame.add(answerto);
            OFrame.setVisible(false);
            OFrame.setVisible(true);
        }
        if(resi.equals("")){
            Double volta = Double.parseDouble(volt);
        Double amper = Double.parseDouble(ampe);
        resi="";
            answer=volta/amper;
            OFrame.remove(answerto);
            OFrame.add(space);
            answerto = new JLabel("The Resistance is "+answer);
            OFrame.add(answerto);
            OFrame.setVisible(false);
            OFrame.setVisible(true);
        }

    }

}

public class OhmsLawCalculator {

    public static void main(String[] args) {
        calcu sef = new calcu();
    }

}
