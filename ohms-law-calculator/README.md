# Ohm's Law Calculator

Application to compute one of Voltage (V), Ampere (I) or Resistance (Ω) based on
the **Ohm’s Law** Physics equation

![Ohm's Law Calculator](documentation/ohms-law-calculator.gif)

# Try it out!

## Run pre-compiled executable from the command line

```sh
java -jar ohms-law-calculator.jar
```

> You may also navigate to the application directory through the file explorer
> to open the executable file

## Compile from source

```sh
# Compile to default filename (ohms-law-calculator.jar)
./compile.sh

# Compile to custom filename
./compile.sh filename.jar
```
