# Java Projects

Collection of University Java Projects

# Requirements

1. [Java](https://www.java.com/en/) version 11 or higher

# In this collection
## [Simple Calculator](simple-calculator)

![Simple Calculator](simple-calculator/documentation/simple-calculator.gif)

## [Simple Circuit Calculator](simple-circuit-calculator)

![Simple Circuit Calculator](simple-circuit-calculator/documentation/simple-circuit-calculator.gif)

## [Ohm's Law Calculator](ohms-law-calculator)

![Ohm's Law Calculator](ohms-law-calculator/documentation/ohms-law-calculator.gif)
